package com.openclassroom.helloworld.service;
import org.springframework.stereotype.Component;

import com.openclassroom.helloworld.model.HelloWorld;

//objet métier, une classe nommée business service
// la classe business service devra etre injectée à l'intérieur d'un autre bean. Donc spring doit détecer que c'est un bean, avec l'annotation @Component
@Component
public class BusinessService {
    public HelloWorld getHelloWorld() {// intancie un objet hello world et le retourne
        HelloWorld hw = new HelloWorld();
        return hw;
    }
}