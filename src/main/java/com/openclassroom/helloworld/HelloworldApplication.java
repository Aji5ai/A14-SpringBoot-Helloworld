package com.openclassroom.helloworld;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.openclassroom.helloworld.model.HelloWorld;
import com.openclassroom.helloworld.service.BusinessService;

@SpringBootApplication
public class HelloworldApplication implements CommandLineRunner {

	// on injecte Businness service, spring va lui même mettre l'instance de cet objet avec l'attribut @Autowired
	@Autowired
	private BusinessService bs; // l'objet bs n'est pas instancié, normal car spring le fait tout seul car défini avec @Component dans son fichier + le autowired
	
	public static void main(String[] args) {
		SpringApplication.run(HelloworldApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// on récupère un objet helloword avec le get
		HelloWorld hw = bs.getHelloWorld();
		// on l'affiche dans la console
		System.out.println(hw);
	}

}
