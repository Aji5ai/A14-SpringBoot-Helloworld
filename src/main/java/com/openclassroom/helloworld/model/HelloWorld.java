package com.openclassroom.helloworld.model;

public class HelloWorld {
    private String value = "Hello World!";

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() { // surcharge la méthode to string de la classe object
        return value;
    }

}
