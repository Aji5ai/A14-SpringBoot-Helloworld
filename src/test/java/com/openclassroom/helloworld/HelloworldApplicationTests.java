package com.openclassroom.helloworld;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.openclassroom.helloworld.service.BusinessService;

@SpringBootTest
class HelloworldApplicationTests {

	@Test
	void contextLoads() {
	}

	//  j’injecte une instance de BusinessService dans un attribut nommé bs. À noter que sans l’annotation @SpringBootTest, cela échouera car sans contexte Spring, impossible de faire de l’injection de dépendances.
	@Autowired
	private BusinessService bs;

	// j’écris ma méthode, sans oublier d’annoter @Test.
	@Test
	public void testGetHelloWorld() {
		String expected = "Hello World!"; // je définis le résultat attendu pour la valeur “Hello World!”.
		String result = bs.getHelloWorld().getValue(); //  je récupère, à travers l’instance du BusinessService, un objet HelloWorld, puis j’appelle la méthode getValue et affecte le résultat dans une variable nommée result.
		assertEquals(expected, result); // grâce à assertEquals, je compare les 2 variables. Si elles sont égales, le test réussit, sinon il échoue.
	}

}
